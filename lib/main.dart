import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First APP',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'FirstCard'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {




  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(

      child: Stack(
      children: <Widget> [
      Container(

        padding: EdgeInsets.fromLTRB(50,30,50,30),
        margin: EdgeInsets.fromLTRB(40,140,40,300),
        width: 300,
        height: 200,
        decoration: BoxDecoration(
            color: Colors.blue,
          borderRadius: BorderRadius.circular(15)
        ),
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,

            children:<Widget> [
            Text("El Mokhtar Aissaoui"),
          Text("Aissaoui@etu.umontpellier.fr"),
            Text("LinkedIn : EL Mokhtar Aissaoui")],

        ),
      ),
        // ignore: prefer_const_constructors
        Positioned(
          left: 120,
            top: 58,
            child:CircleAvatar(radius: 70.0,
          backgroundImage: AssetImage('assets/image.jpg'),)
    ),

      ]
      ),
    ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
